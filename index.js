/**
 * This program is a boliler plate code for the famous tic tac toe game
 * Here box represents one placeholder for either X or a 0
 * We have a 2D array to represent the arrangement of X or O is a grid
 * 0 -> empty box
 * 1 -> box with X
 * 2 -> box with O
 *
 * Below are the tasks which needs to be completed
 * Imagine you are playing with Computer so every alternate move should be by Computer
 * X -> player
 * O -> Computer
 *
 * Winner has to be decided and has to be flashed
 *
 * Extra points will be given for the Creativity
 *
 * Use of Google is not encouraged
 *
 */
const grid = [];
const GRID_LENGTH = 3;
let turn = 'X';

function getWinningMoves() {
    moves = []
    for(var i=0; i<GRID_LENGTH; i++) {
        rmove = []
        cmove = []
        for(var j=0; j<GRID_LENGTH; j++) {
            rmove.push([i,j])
            cmove.push([j,i])
        }
        moves.push(rmove)
        moves.push(cmove)
    }
    xmove = []
    for(var i=0; i<GRID_LENGTH; i++) {
        xmove.push([i, i])
    }
    moves.push(xmove)

    ymove = []
    for(var i=0; i<GRID_LENGTH; i++) {
        ymove.push([i, GRID_LENGTH-i-1])
    }
    moves.push(ymove)

    return moves
}

function initializeGrid() {
    for (let colIdx = 0;colIdx < GRID_LENGTH; colIdx++) {
        const tempArray = [];
        for (let rowidx = 0; rowidx < GRID_LENGTH;rowidx++) {
            tempArray.push(0);
        }
        grid.push(tempArray);
    }
}

function getRowBoxes(colIdx) {
    let rowDivs = '';

    for(let rowIdx=0; rowIdx < GRID_LENGTH ; rowIdx++ ) {
        let additionalClass = 'darkBackground';
        let content = '';
        const sum = colIdx + rowIdx;
        if (sum%2 === 0) {
            additionalClass = 'lightBackground'
        }
        const gridValue = grid[colIdx][rowIdx];
        if(gridValue === 1) {
            content = '<span class="cross">X</span>';
        }
        else if (gridValue === 2) {
            content = '<span class="cross">O</span>';
        }
        rowDivs = rowDivs + '<div colIdx="'+ colIdx +'" rowIdx="' + rowIdx + '" class="box ' +
            additionalClass + '">' + content + '</div>';
    }
    return rowDivs;
}

function getColumns() {
    let columnDivs = '';
    for(let colIdx=0; colIdx < GRID_LENGTH; colIdx++) {
        let coldiv = getRowBoxes(colIdx);
        coldiv = '<div class="rowStyle">' + coldiv + '</div>';
        columnDivs = columnDivs + coldiv;
    }
    return columnDivs;
}

function renderMainGrid() {
    const parent = document.getElementById("grid");
    const columnDivs = getColumns();
    parent.innerHTML = '<div class="columnsStyle">' + columnDivs + '</div>';
}

function onBoxClick() {
    var rowIdx = this.getAttribute("rowIdx");
    var colIdx = this.getAttribute("colIdx");
    let newValue = 1;
    if (grid[colIdx][rowIdx] !=0) {
        alert("Invalid move!")
    }
    else {
        grid[colIdx][rowIdx] = newValue;
        renderMainGrid();
        addClickHandlers();
        setTimeout(function(){
            nextMove(1)
        }, 500)
    }
}

function nextMove(player) {
    var message = "You win!"
    if (player == 2) {
        message = "Computer wins!"
    }
    if(hasWon(player)){
        alert(message)
    } else {
        if (player == 1) {
            computerTurn()
        }
    }
}

function findAllEmptyCells(){
    var emptyCells = []
    for(let r=0; r<GRID_LENGTH; r++){
        for(let c=0; c<GRID_LENGTH; c++){
            if(grid[r][c] == 0){
                emptyCells.push([r, c])
            }
        }
    }
    return emptyCells
}

function getRandom(cells) {
    var l = cells.length;
    var i = Math.floor((Math.random()*l))

    return cells[i]
}
function computerTurn(){
    var emptyCells = findAllEmptyCells()

    var pickOne = getRandom(emptyCells)

    grid[pickOne[0]][pickOne[1]] = 2

    renderMainGrid()
    addClickHandlers()

    setTimeout(function(){
        nextMove(2)
    }, 500)
}

function hasWon(boxValue) {
    var winningMoves = getWinningMoves()
    var ret = false
    for(var i=0; i<winningMoves.length; i++){
        var res = true
        for(var j=0; j<GRID_LENGTH; j++){
            var row = winningMoves[i][j][0]
            var col = winningMoves[i][j][1]
            if (boxValue != grid[row][col]) {
                res = res && false
            } else {
                res = res && true
            }
        }
        if (res){
            return true
        }
    }
    return false
}

function addClickHandlers() {
    var boxes = document.getElementsByClassName("box");
    for (var idx = 0; idx < boxes.length; idx++) {
        boxes[idx].addEventListener('click', onBoxClick, false);
    }
}

initializeGrid();
renderMainGrid();
addClickHandlers();